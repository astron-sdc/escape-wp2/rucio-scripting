#!/usr/bin/env python
from rucio.rse.protocols import protocol
rdt = protocol.RSEDeterministicTranslation()

if __name__ == "__main__":
    print(rdt.path(sys.argv[1], sys.argv[2]))
