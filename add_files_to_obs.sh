# first par is name of dataset, second one is file with list of filenames in it
rucio add-dataset lofar:$1
for fname in `cat $2`
do
rucio attach lofar:$1 fname
done 
