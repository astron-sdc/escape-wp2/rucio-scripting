#!/usr/bin/env python
import hashlib
import sys

def hash(scope, name):
    """
    Given a LFN, turn it into a sub-directory structure using a hash function.
    This takes the MD5 of the LFN and uses the first four characters as a subdirectory
    name.
    :param scope: Scope of the LFN.
    :param name: File name of the LFN.
    :param rse: RSE for PFN (ignored)
    :param rse_attrs: RSE attributes for PFN (ignored)
    :param protocol_attrs: RSE protocol attributes for PFN (ignored)
    :returns: Path for use in the PFN generation.
    """
    hstr = hashlib.md5(('%s:%s' % (scope, name)).encode('utf-8')).hexdigest()
    if scope.startswith('user') or scope.startswith('group'):
        scope = scope.replace('.', '/')
    return '%s/%s/%s/%s' % (scope, hstr[0:2], hstr[2:4], name)

if __name__ == "__main__":
    print(hash(sys.argv[1], sys.argv[2]))
